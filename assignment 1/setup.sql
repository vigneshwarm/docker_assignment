create database test;
use test;

create table users (id int, name varchar(50));

INSERT INTO users (id, name) VALUES(1, 'u1');
INSERT INTO users (id, name) VALUES(2, 'u2');