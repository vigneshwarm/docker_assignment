# README

## Pre-requisites ✅ :
Create an `.env` file matching the `.env.sample` file provided.

```
MYSQL_USERNAME=
MYSQL_ROOT_PASSWORD=
```
## STEPS TO FOLLOW 📝 :

1) Build image from Dockerfile

``` docker
docker build . -t php-shell
```

2) Create a custom network

``` docker
docker network create --driver bridge \
--subnet 182.18.0.1/24 \
--gateway 182.18.0.1 \
custom-network 
```

3) Run a Mysql container
   
``` docker 
docker run --name db -d \
-p 3306:3306 \
--network custom-network \
--env-file .env \
mysql \
--default-authentication-plugin=mysql_native_password
```

4) Get inside the mysql conatiner

``` bash
docker exec -it db /bin/sh
```

5) Create test database and tables

``` bash
docker cp setup.sql db:/
docker exec -it db mysql -u root -p -e 'source setup.sql'
```

1) Run an apache-php container

``` docker
docker run -p 80:80 \
--name php-conatiner -d \
--env-file ./.env \
--network custom-network \
php-shell
```
