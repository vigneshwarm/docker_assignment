<?php

    echo "<h1>Data from DB: </h1>";
    echo "<p>";

    // Get data from env
    $username = getenv("MYSQL_USERNAME");
    $password = getenv("MYSQL_ROOT_PASSWORD");

    // Set up the mysqli client
    $mysqli = new mysqli("db", "$username", "$password", "test");

    // Execute query
    $sql = 'SELECT * FROM users';

    if ($result = $mysqli->query($sql)) {
        while ($data = $result->fetch_object()) {
            $users[] = $data;
        }
    }

    // Display the queried data
    echo "<ul>";
    foreach ($users as $user) {
        echo "<li>";
        echo $user->id . " " . $user->name;
    }
    echo "</ul>";
    
?>
