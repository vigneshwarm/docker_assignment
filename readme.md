# README

## ASSIGNMENT 1

Prepare a custom docker images for the following and deploy the containers using the images.

* Apache2 container to host the custom php application created in the Assignment1. Validate that the apache2 is hosted with the custom application by exposting port `80`.
* MySQL container and expose port `3306`
* Make sure the apache2 container is able to connect to the mysql container port `3306`
  
## ASSIGNMENT 2

* Create a docker volume and mount to the mysql container to store the database data (`/var/lib/mysql/data`)
* Create another docker volume and mount to the apache2 container to store the web application data (`/var/www/html`)
* Create a docker secret to store the mysql db credentials (username and password), so that it can be used to connect to the database from the web application
* Create a docker compose file to deploy apache and mysql containers
