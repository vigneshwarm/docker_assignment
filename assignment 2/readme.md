# README 🧐

## STEPS TO FOLLOW: 📑

* create username.txt
  `echo "root" > username.txt`
* create password.txt
  `echo "example" > password.txt`
* create a volume
  `docker volume create mysql-data`
* set up db in adminer
  * create test database test
  * create users table
  * add few entries
  * or run the `setup.sql`

``` bash
docker cp setup.sql db:/
docker exec -it db mysql -u root -p -e 'source setup.sql'
```
* To run the containers
   `docker compose up -d`
* To stop the containers
   `docker compose down`
